import java.rmi.*;

import java.rmi.server.*;

import java.util.*;

public class ChatServerImpl extends UnicastRemoteObject implements ChatServer {

   Vector clientList = new Vector();

   public ChatServerImpl() throws RemoteException{}

   public static void main(String args[]){

       try{

          System.out.println("ChatServerImpl.main:creating registry");

          ChatServerImpl Server = new ChatServerImpl();
          ChatDualClientImpl Dual = new ChatDualClientImpl();
        	
          Naming.rebind("RMIChatServer", Server);
          Naming.rebind("RMIDualClient", Dual);

          System.out.println("ChatServerImpl is running...");

       } catch (Exception e){

          System.out.println("ChatServerImpl error: " + e.getMessage());

          e.printStackTrace();

       }

   }

   public void addClient(ChatClient client, String name) throws RemoteException{

       clientList.addElement(client);

       say(name + "���� �����ϼ̽��ϴ�.");

       System.out.println("New Client! Number of client = " + (clientList.size()));

   }

   public void disconnect(ChatClient client, String name) throws RemoteException{

       int i = clientList.indexOf(client);

       if (i >= 0){

          say(name + "���� �����ϼ̽��ϴ�.");

          clientList.removeElementAt(i);

          System.out.println("A Client exited! Number of client = " + clientList.size());

          

       }else {

          System.out.println("No such a client in Server! ");

       }

   }

   public void say(String msg) throws RemoteException {

       int numOfConnect = clientList.size();

       for(int i = 0; i < numOfConnect; i++){

          ((ChatClient)clientList.elementAt(i)).printMessage(msg);

       }

   }

}
