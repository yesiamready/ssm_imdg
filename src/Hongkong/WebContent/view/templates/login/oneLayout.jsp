<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<tiles:insertAttribute name="_common" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title><tiles:getAsString name="_title"/></title>
</head>
<body>

<tiles:insertAttribute name="_bodymain" />

</body>

</html>
