<script language="javascript">AC_FL_RunContent = 0;</script>
<script language="javascript"> DetectFlashVer = 0; </script>
<script language="javascript" src="/charts/AC_RunActiveContent.js"></script>
<script language="JavaScript" type="text/javascript">

var requiredMajorVersion = 10;
var requiredMinorVersion = 0;
var requiredRevision = 45;

$(document).ready(function() {
	
	
});

</script>

<div class="container">

<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Map</h3>
  </div>
  <div class="panel-body">
  
  	<div class="row">
  		<div class="col-md-6">
  			<script language="JavaScript" type="text/javascript">
			<!--
			if (AC_FL_RunContent == 0 || DetectFlashVer == 0) {
				alert("This page requires AC_RunActiveContent.js.");
			} else {
				var hasRightVersion = DetectFlashVer(requiredMajorVersion, requiredMinorVersion, requiredRevision);
				if(hasRightVersion) { 
					AC_FL_RunContent(
						'codebase', 'http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,45,2',
						'width', '100%',
						'height', '300',
						'scale', 'noscale',
						'salign', 'TL',
						'bgcolor', '#EEEEEE',
						'wmode', 'opaque',
						'movie', '/charts/charts',
						'src', '/charts/charts',
						'FlashVars', 'library_path=/charts/charts_library&xml_source=/view/map/xml/line_example.xml', 
						'id', 'my_chart',
						'name', 'my_chart',
						'menu', 'true',
						'allowFullScreen', 'true',
						'allowScriptAccess','sameDomain',
						'quality', 'high',
						'align', 'middle',
						'pluginspage', 'http://www.macromedia.com/go/getflashplayer',
						'play', 'true',
						'devicefont', 'false'
						); 
				} else { 
					var alternateContent = 'This content requires the Adobe Flash Player. '
					+ '<u><a href=http://www.macromedia.com/go/getflash/>Get Flash</a></u>.';
					document.write(alternateContent); 
				}
			}
			// -->
			</script>
			<noscript>
				<P>This content requires JavaScript.</P>
			</noscript>	  		
  		</div>
  		<div class="col-md-6">
  			<script language="JavaScript" type="text/javascript">
			<!--
			if (AC_FL_RunContent == 0 || DetectFlashVer == 0) {
				alert("This page requires AC_RunActiveContent.js.");
			} else {
				var hasRightVersion = DetectFlashVer(requiredMajorVersion, requiredMinorVersion, requiredRevision);
				if(hasRightVersion) { 
					AC_FL_RunContent(
						'codebase', 'http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,45,2',
						'width', '100%',
						'height', '300',
						'scale', 'noscale',
						'salign', 'TL',
						'bgcolor', '#EEEEEE',
						'wmode', 'opaque',
						'movie', '/charts/charts',
						'src', '/charts/charts',
						'FlashVars', 'library_path=/charts/charts_library&xml_source=/view/map/xml/line_example2.xml', 
						'id', 'my_chart2',
						'name', 'my_chart2',
						'menu', 'true',
						'allowFullScreen', 'true',
						'allowScriptAccess','sameDomain',
						'quality', 'high',
						'align', 'middle',
						'pluginspage', 'http://www.macromedia.com/go/getflashplayer',
						'play', 'true',
						'devicefont', 'false'
						); 
				} else { 
					var alternateContent = 'This content requires the Adobe Flash Player. '
					+ '<u><a href=http://www.macromedia.com/go/getflash/>Get Flash</a></u>.';
					document.write(alternateContent); 
				}
			}
			// -->
			</script>
			<noscript>
				<P>This content requires JavaScript.</P>
			</noscript>	 
  		</div>
  	</div>
  	
  	<br>
  	<br>
  
  	<table class="table table-bordered table-condensed table-hover">
        <thead>
          <tr>
            <th width="">#</th>
            <th width="">Members</th>
            <th width="">Entries</th>
            <th width="">Entry Memory</th>
            <th width="">Backups</th>
            <th width="">Backup Memory</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>192.168.1.110:5701</td>
            <td>19725</td>
            <td>4.57 MB</td>
            <td>19871</td>
            <td>4.6 MB</td>
          </tr>
          <tr>
            <td>2</td>
            <td>192.168.1.110:5702</td>
            <td>19873</td>
            <td>4.6 MB</td>
            <td>19676</td>
            <td>4.56 MB</td>
          </tr>
          <tr>
            <td>3</td>
            <td>192.168.1.110:5703</td>
            <td>20003</td>
            <td>4.63 MB</td>
            <td>19972</td>
            <td>4.63 MB</td>
          </tr>
          <tr>
            <td>4</td>
            <td>192.168.1.110:5704</td>
            <td>20281</td>
            <td>4.7 MB</td>
            <td>20490</td>
            <td>4.75 MB</td>
          </tr>
          <tr>
            <td>5</td>
            <td>TOTAL</td>
            <td>100001</td>
            <td>18.5 MB</td>
            <td>100001</td>
            <td>18.5 MB</td>
          </tr>          
        </tbody>
      </table>  
  </div>
</div>

</div>