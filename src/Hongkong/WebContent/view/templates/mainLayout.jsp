<%@ page contentType="text/html; charset=euc-kr" %>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<tiles:insertAttribute name="_common" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title><tiles:getAsString name="_title"/></title>
<link href="/css/bootstrap.css" rel="stylesheet">
<link href="/css/navbar-fixed-top.css" rel="stylesheet">
<link href="/css/jquery.custombox.css" rel="stylesheet">
<link href="/css/demo.css" rel="stylesheet">
<link href="/css/jquery-ui.css" rel="stylesheet">
</head>
<body>

<tiles:insertAttribute name="_header" />
<tiles:insertAttribute name="_body" />

<script type="text/javascript" src="/js/jquery-1.11.1.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/jquery.custombox.js"></script>
<script language="javascript" src="/js/jquery-ui-1.10.4.custom.js"></script>
</body>
</html>
