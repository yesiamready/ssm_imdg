package com.hongkong.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

public class LoginProcController implements Controller {

	@Override
	public ModelAndView handleRequest(HttpServletRequest req,
			HttpServletResponse res) throws Exception {
		
		System.out.println("LoginProcController Called");
		
		String id = req.getParameter("id");
		String pw = req.getParameter("pw");
		
		System.out.println("Id : " + id);
		System.out.println("Pw : " + pw);
		
		boolean flag = false;
		
		if(id.equals("ssm") && pw.equals("ssm123")) {
		
			flag = true;
			
			HttpSession session = req.getSession();					
			session.setAttribute("sess_id", id);
		
			
			System.out.println("Login Success.");
		}
		
		req.setAttribute("flag", flag);
		
		ModelAndView mv = new ModelAndView("_loginProc");
		return mv;
	}

}
