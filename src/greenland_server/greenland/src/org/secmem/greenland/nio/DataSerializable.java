package  org.secmem.greenland.nio;

//import java.io.IOException;

public interface DataSerializable {
//	public abstract void writeData(ObjectDataOutput paramObjectDataOutput)
//			throws IOException;
//
//	public abstract void readData(ObjectDataInput paramObjectDataInput)
//			throws IOException;
	public abstract void writeData(String paramString);
//			throws IOException;

	public abstract void readData(String paramString);
//			throws IOException;

}
