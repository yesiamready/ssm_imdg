package org.secmem.greenland.nio;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

import org.secmem.greenland.datastructure.KeysProcess;
import org.secmem.greenland.datastructure.SortedSetsProcess;

public class TestEngine {
	public static void main(String args[]) throws IOException, 
			ClassNotFoundException {
	
		MemoryManager mmg = new MemoryManager(1024*1024*465);
		KeysProcess k1 = new KeysProcess(mmg);
		SortedSetsProcess s1 = new SortedSetsProcess(mmg);
		PullDirectBuffer pdb = new PullDirectBuffer(mmg);
		MemoryDump lfu = new MemoryDump(mmg);
		String str = "test";
		for(int i=0; i < 2000; i++) {
			str += i;
			k1.SET(str, toByteArray(i));
		}
		System.out.println(mmg.getMdList().size());
		System.out.println(mmg.getUsingMemorySize());
		HashMap<String, Object> hash = mmg.readMemory(10000,true);
		System.out.println(mmg.getUsingMemorySize());
		ArrayList<ManagedData> mdList;
		ManagedData md;
		mdList = (ArrayList) hash.get("mdList");
		ArrayList<byte[]> byteArrayList = new ArrayList<byte[]>();
		
		for(int j=0; j < mdList.size(); j++) {
			md = mdList.get(j);
			System.out.println(md.getKey()+ " "+ md.getPos());
		}
		System.out.println(mmg.getMdList().size());
		mmg.getMdList().clear();
		System.out.println(mmg.getMdList().size());
		System.out.println(mmg.getUsingMemorySize());
		mmg.writeMemory(hash);
		System.out.println(mmg.getUsingMemorySize());
		System.out.println(mmg.getMdList().size());
		System.out.println(mmg.getMdBackUpList().size());
		mmg.writeBackUpMemory(hash);
		System.out.println(mmg.getMdBackUpList().size());
		
		/*
		k1.SET("key1", toByteArray("value1"));		k1.SET("key2", toByteArray("value2"));
		k1.SET("key3", toByteArray("value3"));
		k1.SET("key4", toByteArray("value4"));
		k1.SET("key5", toByteArray("value5"));
		
		ManagedData md1 = (ManagedData)k1.GET("key1");
		ManagedData md2 = (ManagedData)k1.GET("key2");
		ManagedData md3 = (ManagedData)k1.GET("key3");
		ManagedData md4 = (ManagedData)k1.GET("key4");
		ManagedData md5 = (ManagedData)k1.GET("key5");
		
		System.out.println(md1.toString());
		System.out.println(md2.toString());
		System.out.println(md3.toString());
		System.out.println(md4.toString());
		System.out.println(md5.toString());
		
		md1 = (ManagedData)k1.GET("key1");
		md3 = (ManagedData)k1.GET("key3");
		
		lfu.executeLFU(1);
		
		System.out.println(md1.toString());
		System.out.println(md3.toString());
		
		System.out.println(k1.EXISTS("key1"));
		System.out.println(k1.EXISTS("key2"));
		System.out.println(k1.EXISTS("key3"));
		System.out.println(k1.EXISTS("key4"));
		System.out.println(k1.EXISTS("key5"));
		
		lfu.readMemoryDmp("C:\\memory.dmp");
		
		System.out.println(k1.EXISTS("key1"));
		System.out.println(k1.EXISTS("key2"));
		System.out.println(k1.EXISTS("key3"));
		System.out.println(k1.EXISTS("key4"));
		System.out.println(k1.EXISTS("key5"));
		
		System.out.println(k1.EXISTS("key6"));
		
		
		s1.ZADD("key1", "subkey1", 3);
		s1.ZADD("key1", "subkey2", 2);
		s1.ZADD("key1", "subkey3", 4);
		s1.ZADD("key1", "subkey4", 1);
		s1.ZADD("key1", "subkey5", 5);
		
		System.out.println(s1.ZRANK("key1", "subkey2"));
		System.out.println(s1.ZRANK("key1", "subkey1"));
		System.out.println(s1.ZRANK("key1", "subkey4"));
		System.out.println(s1.ZRANK("key1", "subkey5"));
		System.out.println(s1.ZRANK("key1", "subkey3"));
		
		System.out.println();
		System.out.println(s1.ZCARD("key1"));
		s1.ZREM("key1", "subkey1");
		s1.ZREM("key1", "subkey2");
		System.out.println(s1.ZCARD("key1"));
		System.out.println();
		s1.ZADD("key1", "subkey3", 6);
		
		System.out.println(s1.ZRANK("key1", "subkey2"));
		System.out.println(s1.ZRANK("key1", "subkey1"));
		System.out.println(s1.ZRANK("key1", "subkey4"));
		System.out.println(s1.ZRANK("key1", "subkey5"));
		System.out.println(s1.ZRANK("key1", "subkey3"));
		
		s1.ZRANGEBYSCORE("key1", 2, 6);
		
		System.out.println(s1.ZCARD("key1"));
		s1.ZRANGE("key1", 0	, 2);
		s1.ZRANGEWITHSCORE("key1", 0	, 1);
		*/
		
	}
		
		/*
		
		
		
		
		System.out.println(md1.toString());
		System.out.println(md2.toString());
		System.out.println(md3.toString());
		System.out.println(md4.toString());
		System.out.println(md5.toString());
		
		k1.DEL("key4");
		k1.DEL("key5");
		
		for(int i=0; i < mmg.getKeys().size(); i++) {
			System.out.println(mmg.getKeys().get("key"+i));
		}
		
		pdb.pullDirectBuffer();
		
		k1.SET("key6", toByteArray("value6"));
		k1.SET("key7", toByteArray("value7"));
		
		
		ManagedData md11 = (ManagedData)k1.GET("key1");
		ManagedData md12 = (ManagedData)k1.GET("key2");
		ManagedData md13 = (ManagedData)k1.GET("key3");
		ManagedData md14 = (ManagedData)k1.GET("key4");
		ManagedData md15 = (ManagedData)k1.GET("key5");
		ManagedData md16 = (ManagedData)k1.GET("key6");
		ManagedData md17 = (ManagedData)k1.GET("key7");
		
		System.out.println(md11.toString());
		System.out.println(md12.toString());
		System.out.println(md13.toString());
//		System.out.println(md14.toString());
//		System.out.println(md15.toString());
		System.out.println(md16.toString());
		System.out.println(md17.toString());
		*/
		
		/*
		int mb = 1024*1024;
        
        //Getting the runtime reference from system
        Runtime runtime = Runtime.getRuntime();
         
        System.out.println("##### Heap utilization statistics [MB] #####");
         
        //Print used memory
        System.out.println("Used Memory:"
            + (runtime.totalMemory() - runtime.freeMemory()) / mb);
 
        //Print free memory
        System.out.println("Free Memory:"
            + runtime.freeMemory() / mb);
         
        //Print total available memory
        System.out.println("Total Memory:" + runtime.totalMemory() / mb);
 
        //Print Maximum available memory
        System.out.println("Max Memory:" + runtime.maxMemory() / mb);
		
		int count=-1;
		ByteBuffer buf = ByteBuffer.allocate(50*1024*1024);
		
		*/
		
		
		/*
		for(int i=0; i < 10000000; i++ ) {
			k1.SET("key"+i, toByteArray(""+i));
			
			if( i%1000000 == 0) {
				count++;
				long time = Calendar.getInstance().getTimeInMillis() / 1000;
				System.out.println(time);
				System.out.println(mmg.getUsingMemorySize() + "    " + count);
				 //Print used memory
		        System.out.println("Used Memory:"
		            + (runtime.totalMemory() - runtime.freeMemory()) / mb);
		 
		        //Print free memory
		        System.out.println("Free Memory:"
		            + runtime.freeMemory() / mb);
		         
		        //Print total available memory
		        System.out.println("Total Memory:" + runtime.totalMemory() / mb);
		 
		        //Print Maximum available memory
		        System.out.println("Max Memory:" + runtime.maxMemory() / mb);
		        
			}
			*/

		/*
		k1.SET("key1", toByteArray("value1"));
		k1.SET("key2", toByteArray("value2"));
		k1.SET("key3", toByteArray("value3"));
		
		ManagedData md1 = (ManagedData)k1.GET("key1");
		k1.GET("key2");
		k1.GET("key2");
		ManagedData md2 = (ManagedData)k1.GET("key2");
		
		System.out.println(md1.getUseCount());
		System.out.println(md2.getUseCount());
		*/
//		s1.ZADD("key1", "subkey1", 3);
//		s1.ZADD("key1", "subkey2", 2);
//		s1.ZADD("key1", "subkey3", 4);
//		s1.ZADD("key1", "subkey4", 1);
//		s1.ZADD("key1", "subkey5", 5);
//		
//		System.out.println(s1.ZRANK("key1", "subkey2"));
//		System.out.println(s1.ZRANK("key1", "subkey1"));
//		System.out.println(s1.ZRANK("key1", "subkey4"));
//		System.out.println(s1.ZRANK("key1", "subkey5"));
//		System.out.println(s1.ZRANK("key1", "subkey3"));
//		
//		s1.ZREM("key1", "subkey1");
//		s1.ZREM("key1", "subkey2");
//		s1.ZADD("key1", "subkey3", 10);
//		
//		System.out.println(s1.ZRANK("key1", "subkey2"));
//		System.out.println(s1.ZRANK("key1", "subkey1"));
//		System.out.println(s1.ZRANK("key1", "subkey4"));
//		System.out.println(s1.ZRANK("key1", "subkey5"));
//		System.out.println(s1.ZRANK("key1", "subkey3"));
		
//		HashesProcess hashesDT = new HashesProcess(mmg);
//		
//		hashesDT.HSET("key1", "subKey1", toByteArray("1"));
//		hashesDT.HSET("key1", "subKey2", toByteArray("2"));
//		hashesDT.HSET("key1", "subKey3", toByteArray("3"));
//		
//		ManagedData md;
//		md = (ManagedData)hashesDT.HGET("key1", "subKey2");
//		System.out.println(md.toString());
		
		/*
		int a =1;
		System.out.println(a);
		byte b = (byte)a;
		System.out.println(b);
		
		MemoryManager mmg = new MemoryManager(1024*1024*400);
		KeysProcess keysDT = new KeysProcess(mmg);
		HashesProcess hashesDT = new HashesProcess(mmg);
		
		
		keysDT.SET("key1", "value1");
		keysDT.SET("key2", "value22222");
		keysDT.SET("key3", "value3333333333");
		keysDT.SET("key5", "55555");
		
		keysDT.DEL("key2");
		
		keysDT.GET("key2");
		keysDT.GET("key1");
		keysDT.GET("key3");
		
		keysDT.SETBACKUP("1", "123");
		keysDT.SETBACKUP("2", "asd123");
		keysDT.SETBACKUP("3", "asdfasdf123");
		
		System.out.println("key1 exists : "+ keysDT.EXISTS("key1"));
		System.out.println("key2 exists : "+ keysDT.EXISTS("key2"));
		System.out.println("key3 exists : "+ keysDT.EXISTS("key3"));
		System.out.println("key4 exists : "+ keysDT.EXISTS("key4"));
		System.out.println("key5 exists : "+ keysDT.EXISTS("key5"));
		System.out.println("key6 exists : "+ keysDT.EXISTS("key6"));
		
		keysDT.RENAMENX("key1", "key4");
		
		keysDT.EXPIRE("key5", 5);
		
		keysDT.RENAMENX("key5", "key6");
		//EXPIRE 함수안에서 EXISTS로 지운담에 확인
//		System.out.println("key4 expire 5");
		keysDT.EXPIRE("key4", 5);
//		System.out.println("key2 expire 5");
		keysDT.EXPIRE("key2", 5);
//		System.out.println("key2 persist");
		keysDT.PERSIST("key2");
		
		
		keysDT.DEL("key1");
		keysDT.DEL("key2");
		keysDT.DEL("key3");
		keysDT.DEL("key5");
		keysDT.DEL("key6");
		
		System.out.println("key1 exists : "+ keysDT.EXISTS("key1"));
		System.out.println("key2 exists : "+ keysDT.EXISTS("key2"));
		System.out.println("key3 exists : "+ keysDT.EXISTS("key3"));
		System.out.println("key4 exists : "+ keysDT.EXISTS("key4"));
		System.out.println("key5 exists : "+ keysDT.EXISTS("key5"));
		System.out.println("key6 exists : "+ keysDT.EXISTS("key6"));
		
		
//		hashesDT.HSET("parentKey1", "key1", "value1");
//		hashesDT.HSET("parentKey1", "key2", "value1212");
//		hashesDT.HSET("parentKey2", "key2", "value22");
//		hashesDT.HSET("parentKey3", "key3", "value333");
//		
//		ManagedData hMd12 = (ManagedData) hashesDT.HGET("parentKey1", "key2");
//		System.out.println("hMd12: " + hMd12.toString());
//		ManagedData hMd2 = (ManagedData) hashesDT.HGET("parentKey2", "key2");
//		System.out.println("hMd2 : " + hMd2.toString());
//		ManagedData hMd1 = (ManagedData) hashesDT.HGET("parentKey1", "key1");
//		System.out.println("hMd1 : " + hMd1.toString());
//		ManagedData hMd3 = (ManagedData) hashesDT.HGET("parentKey3", "key3");
//		System.out.println("hMd3 : " + hMd3.toString());
		
//		System.out.println();System.out.println();
//		keysDT.SETBACKUP("key111", "aaa");
//		keysDT.SETBACKUP("key222", "aaa");
//		keysDT.SETBACKUP("key333", "aaa");
//		
		System.out.println("Using memory : "+ mmg.getUsingMemorySize());
		System.out.println("Using backup memory : "+ mmg.getUsingBackUpMemorySize());
		*/
	
	public static byte[] toByteArray(Object value) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		ObjectOutputStream os = new ObjectOutputStream(out);
		os.writeObject(value);
		byte[] objectBytes = out.toByteArray();
//		System.out.println(objectBytes.length);
		return objectBytes;
	}

}


class T implements Comparable<T>{
	String key;
	int rank;
	
	T(String key, int rank) {
		this.key = key;
		this.rank = rank;
	}

	@Override
	public int compareTo(T o) {
		T t = (T)o;
        
        if (t.rank > rank)
                     return -1;
        else if (t.rank == rank)
                     return 0;
        else 
                     return 1;
		
	}
}