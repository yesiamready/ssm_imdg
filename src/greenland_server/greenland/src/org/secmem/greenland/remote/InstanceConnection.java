package org.secmem.greenland.remote;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.ServerNotActiveException;
import java.util.HashMap;

import org.secmem.greenland.config.ServerInfo;
import org.secmem.greenland.exception.ServerLockingException;
import org.secmem.greenland.message.MessageManager;

public interface InstanceConnection extends Remote {
	public final static int DATATYPE_PRIMARY = 0;
	public final static int DATATYPE_BACKUP = 1;
	
	
	public int checkAlive() throws RemoteException;
	public Boolean findData(String key) throws RemoteException, ServerLockingException, ServerNotActiveException;
	public Boolean findData(String parentKey, String key) throws RemoteException, ServerLockingException, ServerNotActiveException;
	public int sendRawData(String key, Byte [] data) throws RemoteException;
	public CommandBuilder getCommandBuilder() throws RemoteException, ServerLockingException, ServerNotActiveException;
	public int setCommandBuilder(CommandBuilder builder) throws RemoteException;
	public int setServerInfo(ServerInfo info) throws RemoteException;
	public ServerInfo getServerInfo() throws RemoteException;
	public MessageManager getMessageManager() throws RemoteException;
	public void setMessageManager(MessageManager message) throws RemoteException;
	
	public Boolean startLocking() throws RemoteException;
	public Boolean releaseLocking() throws RemoteException;
	
	public Boolean clearBackupMemory() throws RemoteException;
	public Boolean defragment() throws RemoteException;
	
	public HashMap<String, Object> getDataWithSize(int type, long size, boolean removeData) throws RemoteException;
	public int putDataWithSize(int type, HashMap<String, Object> data) throws RemoteException;
	
	public int setServerIndex(int type, int index) throws RemoteException;
	
	public void forceMemoryUpdate() throws RemoteException;
}
