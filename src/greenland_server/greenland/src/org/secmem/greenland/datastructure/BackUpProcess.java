package org.secmem.greenland.datastructure;

import java.util.HashMap;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

import org.secmem.greenland.nio.ManagedData;
import org.secmem.greenland.nio.MemoryManager;

public class BackUpProcess {
	private MemoryManager mmg;
	
	public BackUpProcess(MemoryManager mmg) {
		this.mmg = mmg;
	}
	
	public boolean EXISTSBACKUP(int type, String key, String subKey) {
		if(type == MemoryManager.TYPE_KEYS) {
			return mmg.getKeys().containsKey(key);
			
		}else if(type == MemoryManager.TYPE_HASHES) {
			if(mmg.getHashes().containsKey(key)) {
				if(mmg.getHashes().get(key).containsKey(subKey)) {
					return true;
				}
			}
			
			return false;
			
		}else if(type == MemoryManager.TYPE_SORETEDSETS) {
			if(mmg.getSortedSet().containsKey(key)) {
				Iterator<ManagedData> it = mmg.getSortedSet().get(key).iterator();
				while(it.hasNext()) {
					ManagedData tempMd = it.next();
					if(tempMd.getSubKey().equalsIgnoreCase(subKey) ) {
						return true;
					}
				}
			}
		}
		
		return false;
	}
	
	/**
	 * SETBACKUP 함수 구현
	 * 
	 * @param int type, String key, String subKey, Object value
	 * @return int
	 * @author 송원근
	 */
	public int SETBACKUP(int type, String key, String subKey, Object value ) {
		
		byte[] objectBytes = (byte[])value;
		
		if(type == MemoryManager.TYPE_KEYS) {
			if(!EXISTSBACKUP(type, key, subKey)){
				if (mmg.getCurPosition() >= mmg.position()) {
					mmg.putBackUp(objectBytes);		//putBackUp() 메소드로 넣음
					ManagedData md = new ManagedData(MemoryManager.TYPE_KEYS, objectBytes.length,
							mmg.getCurPosition(), key);
					// BackUp ArrayList 에 ManagedData클래스를 넣는다.
					mmg.getMdBackUpList().add(md);
					mmg.getKeys().put(key, md);
					mmg.setCurPosition(mmg.position());
					
					System.out.println("SETBACKUP key :"+ key);
					System.out.println(md.toString());
					
					return 1;
				}
			}else {
				DELBACKUP(type, key, subKey);
				SETBACKUP(type, key, subKey, value);
			}
			
		}else if(type == MemoryManager.TYPE_HASHES) {
			if(!EXISTSBACKUP(type, key, subKey)){
				if (mmg.getCurPosition() >= mmg.position()) {
					mmg.putBackUp(objectBytes);
					
					ManagedData md = new ManagedData(type, objectBytes.length,
							mmg.getCurPosition(), key, subKey);
					
					// ArrayList 에 ManagedData클래스를 넣는다.
					mmg.getMdBackUpList().add(md);
				
					//parentKey 가 존재한다면
					if(mmg.getHashes().containsKey(key)) {
						mmg.getHashes().get(key).put(subKey, md);
					}else {
						HashMap<String, ManagedData> tempHash = new HashMap<String, ManagedData>();
						tempHash.put(subKey, md);
						mmg.getHashes().put(key, tempHash);
					}
					mmg.setCurPosition(mmg.position());
					
					return 1;
					
				} 
			} else {
				DELBACKUP(type, key, subKey);
				SETBACKUP(type, key, subKey, value);
			}
			
			return 0;
			
		}else if(type == MemoryManager.TYPE_SORETEDSETS) {
			ManagedData md = new ManagedData(MemoryManager.TYPE_SORETEDSETS, key, subKey,(Integer)value);
			
			if(mmg.getSortedSet().containsKey(key)) {
				Iterator<ManagedData> it = mmg.getSortedSet().get(key).iterator();
				while(it.hasNext()) {
					ManagedData tempMd = it.next();
					if(tempMd.getSubKey().equalsIgnoreCase(subKey) ) {
						//key,subkey 가 중복이면 벨류를 업데이트해준다.
						DELBACKUP(type, key, subKey);
						SETBACKUP(type, key, subKey, value);
						return 1;
					}
				}
				//중복이 없을 시에는 
				mmg.getSortedSet().get(key).add(md);		//sortedSet add
				mmg.getMdBackUpList().add(md);				//mdBackUpList add
				return 1;
			} else {
				SortedSet<ManagedData> s = new TreeSet<ManagedData>();
				s.add(md);
				mmg.getSortedSet().put(key, s);
				mmg.getMdBackUpList().add(md);
				return 1;
			}
		}
		
		return 0;
	}
	
	/**
	 * DELBACKUP 함수 구현
	 * 
	 * @param int type, String key, String subKey, Object value
	 * @return Object
	 * @author 송원근
	 */
	public Object  DELBACKUP(int type, String key, String subKey) {
		if(type == MemoryManager.TYPE_KEYS) {
			if(EXISTSBACKUP(type, key, subKey)) {
				// Keys 에서 remove
				ManagedData md = mmg.getKeys().remove(key);
				// Backup ArrayList 에서 remove
				mmg.getMdBackUpList().remove(md);
				mmg.delBackUp(md.getPos(), md.getBufferSize());
				return md;
			}
			
		}else if(type == MemoryManager.TYPE_HASHES) {
			if(EXISTSBACKUP(type, key, subKey)) {
				ManagedData md = (ManagedData)mmg.getHashes().get(key).get(subKey);
				//hashes의 value(HashMap) 에서 remove
				mmg.getHashes().get(key).remove(subKey);
				//remove 해서 0이 된다면 hashes에서 지운다.
				if(mmg.getHashes().get(key).size() == 0) {
					mmg.getHashes().remove(key);
				}
				//ArrayList 에서 remove
				mmg.getMdBackUpList().remove(md);
				mmg.delBackUp(md.getPos(), md.getBufferSize());
				return md;
			}
				
			return null;
		}else if(type == MemoryManager.TYPE_SORETEDSETS) {
			if(EXISTSBACKUP(type, key, subKey)) {
				ManagedData md;
				Iterator<ManagedData> it = mmg.getSortedSet().get(key).iterator();
				while(it.hasNext()) {
					md = it.next();
					if(md.getSubKey().equalsIgnoreCase(subKey)) {
						mmg.getSortedSet().get(key).remove(md);		//sorted Set 에서 remove
						mmg.getMdBackUpList().remove(md);						//mdBackUpList 에서 remove
						//지우고 나서 해당 키값에 데이터가 없다면 키도 지워준다.
						if(mmg.getSortedSet().get(key).size() == 0) {
							mmg.getSortedSet().remove(key);
						}
						return md;
					}
				}
			}
		}
		
		return null;
	}
	
	/**
	 * GETBACKUP 함수 구현
	 * 
	 * @param int type, String key, String subKey
	 * @return Object
	 * @author 송원근
	 */
	public Object GETBACKUP(int type, String key, String subKey ) {
		if(type == MemoryManager.TYPE_KEYS) {
			if(EXISTSBACKUP(type, key, subKey)){
				ManagedData md = mmg.getKeys().get(key);
				int tempCount = md.getUseCount()+1;
				md.setUseCount(tempCount);
				return md;
			}
			
		}else if(type == MemoryManager.TYPE_HASHES) {
			if(EXISTSBACKUP(type, key, subKey)) {
				ManagedData md = mmg.getHashes().get(key).get(subKey);
				int tempCount = md.getUseCount()+1;
				md.setUseCount(tempCount);
				return md;
			}
			
		}else if(type == MemoryManager.TYPE_SORETEDSETS) {
			if(EXISTSBACKUP(type, key, subKey)) {
				Iterator<ManagedData> it = mmg.getSortedSet().get(key).iterator();
				ManagedData md;
				while(it.hasNext()) {
					md = it.next();
					if(md.getSubKey().equalsIgnoreCase(subKey)){
						int tempCount = md.getUseCount()+1;
						md.setUseCount(tempCount);
						return md;
					}
				}
			}
		}
		
		return null;
	}

}
