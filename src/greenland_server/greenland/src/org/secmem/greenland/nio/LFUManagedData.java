package org.secmem.greenland.nio;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class LFUManagedData extends ManagedData implements Serializable{
	private int type; // 0 : Keys, 1 : Hashes, 2 : Sorted Sets
	private String key; // key
	private String subKey;
	// private byte[] buffer; //data 는 directBuffer로 들어감
	private int bufferSize;
	private int pos;
	private boolean expireFlag;
	private int useCount = 0;
	private int sortedSetScore;
	
	//LFUManagedData add
	private byte[] value;
	
	public LFUManagedData(ManagedData md, byte[] value) {
		this.type = md.getType();
		this.key = md.getKey();
		this.subKey = md.getSubKey();
		this.bufferSize = md.getBufferSize();
		this.pos = md.getPos();
		this.expireFlag = md.isExpireFlag();
		this.useCount = md.getUseCount();
		this.sortedSetScore = md.getSortedSetScore();
		this.value = value;
	}
	
	public ManagedData returnManagedData() {
		return new ManagedData(type, key, subKey, bufferSize, useCount);
	}
	
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getSubKey() {
		return subKey;
	}
	public void setSubKey(String subKey) {
		this.subKey = subKey;
	}
	public int getBufferSize() {
		return bufferSize;
	}
	public void setBufferSize(int bufferSize) {
		this.bufferSize = bufferSize;
	}
	public int getPos() {
		return pos;
	}
	public void setPos(int pos) {
		this.pos = pos;
	}
	public boolean isExpireFlag() {
		return expireFlag;
	}
	public void setExpireFlag(boolean expireFlag) {
		this.expireFlag = expireFlag;
	}
	public int getUseCount() {
		return useCount;
	}
	public void setUseCount(int useCount) {
		this.useCount = useCount;
	}
	public int getSortedSetScore() {
		return sortedSetScore;
	}
	public void setSortedSetScore(int sortedSetScore) {
		this.sortedSetScore = sortedSetScore;
	}
	
	public byte[] getValue() {
		return value;
	}
	public void setValue(byte[] value) {
		this.value = value;
	}

	public String toString() {
		return super.toString()+" LFU";
	}
	
}
