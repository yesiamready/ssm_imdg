package  org.secmem.greenland.nio;

import java.io.Serializable;
import java.nio.Buffer;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.SortedSet;

import org.secmem.greenland.datastructure.ExpireObject;

public class MemoryManager implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public final static int TYPE_KEYS = 0;
	public final static int TYPE_HASHES = 1;
	public final static int TYPE_SORETEDSETS = 2;
	
	private DirectBuffer directBuf;
	private long usingMemorySize=0;
	private long usingBackUpMemorySize=0;
	private int curPosition=0;
	
	// keys
	private HashMap<String, ManagedData> keys;
	// hashes
	private HashMap<String, HashMap<String, ManagedData>> hashes;
	// sortedSet
	private HashMap<String, SortedSet<ManagedData>> sortedSet;

	private ArrayList<ManagedData> mdList;
	private ArrayList<ManagedData> mdBackUpList;
	private ArrayList<ExpireObject> epList;
	private ArrayList<byte[]> memoryDumpLengthList;
	private ArrayList<String> memoryDumpKeyList;

	public MemoryManager(int size) {
		directBuf = new DirectBuffer(size);
		// keys
		keys = new HashMap<String, ManagedData>();
		// hashes
		hashes = new HashMap<String, HashMap<String, ManagedData>>();
		// sortedSet
		sortedSet = new HashMap<String, SortedSet<ManagedData>>();

		mdList = new ArrayList<ManagedData>();
		mdBackUpList = new ArrayList<ManagedData>();
		epList= new ArrayList<ExpireObject>();
		memoryDumpLengthList = new ArrayList<byte[]>();
		memoryDumpKeyList = new ArrayList<String>();
	}
	
	/**
	 * readMemory 함수 구현
	 * 일정 메모리를 읽어 오는 함수, 읽은 메모리는 지우고 리턴한다.
	 * @param long size, boolean isRemove
	 *  isRemove 변수로 해당 데이터를 지우고 읽어 올지 그냥 읽어 올지 정한다.
	 * @return HashMap<String, Object>
	 * @author 송원근
	 */
	public HashMap<String, Object> readMemory(long size, boolean isRemove) {
		long tempSize=0;
		HashMap<String, Object> retHashMap = new HashMap<String, Object>();
		ArrayList<ManagedData> retMdList = new ArrayList<ManagedData>();
		ArrayList<byte[]> retByteArrayList = new ArrayList<byte[]>();
		ManagedData tempMd;
		
		int curPosition = position();
				
		int tempMdListSize = mdList.size();
		for(int i=0; i < tempMdListSize;i++) {
			if(tempSize >= size) {
				retHashMap.put("mdList", retMdList);
				retHashMap.put("byteArrayList", retByteArrayList);
				retHashMap.put("size", tempSize);
				
				if(isRemove) {
					for(ManagedData md : retMdList){
						mdList.remove(md);
					}
				}
			
				System.out.println(curPosition);
				position(curPosition);
				
				return retHashMap;
			}
			
			tempMd = mdList.get(i);
			String tempKey;
			Iterator<ManagedData> it;
			if(tempMd.getType() == 0) {			// keys
				retMdList.add(tempMd);
				// del로 해서 usingMemory를 조절
				if(isRemove) {
					retByteArrayList.add(del(tempMd.getPos(), tempMd.getBufferSize()));
					keys.remove(tempMd.getKey());
				}else {
					retByteArrayList.add(get(tempMd.getPos(), tempMd.getBufferSize()));
				}
				
				tempSize += tempMd.getBufferSize();
//				mdList.remove(tempMd);
				
			}else if(tempMd.getType() == 1) {	// hashes
				tempKey = tempMd.getKey();
				Collection<ManagedData> col = hashes.get(tempKey).values();
				it = col.iterator();
				
				ManagedData tempHashesMd;
				// hashes는 키당 subKey들이 묶여잇으므로 한번에 처리
				while(it.hasNext()) {
					tempHashesMd = it.next();
					
					retMdList.add(tempHashesMd);
					// del로 해서 usingMemory를 조절
					if(isRemove) {
						retByteArrayList.add(del(tempHashesMd.getPos(), tempHashesMd.getBufferSize()));
					}else {
						retByteArrayList.add(get(tempHashesMd.getPos(), tempHashesMd.getBufferSize()));						
					}
					tempSize += tempHashesMd.getBufferSize();
					//mdList 에서 삭제
//					mdList.remove(tempHashesMd);
				}
				
				if(isRemove) {
					// subKey들을 모두 뺏으므로 key를 삭제
					hashes.remove(tempKey);
				}
//				mdList.remove(tempMd);
				
			}else if(tempMd.getType() == 2) {	// sorted sets
				//directbuffer를 사용하지 않으므로 패스한다.
				
				/*
				tempKey = tempMd.getKey();
				it = sortedSet.get(tempKey).iterator();
				
				ManagedData tempSortedSetsMd;
				while(it.hasNext()) {
					tempSortedSetsMd = it.next();
					
					retMdList.add(tempSortedSetsMd);
					tempScore = tempSortedSetsMd.getSortedSetScore();
					retByteLinkedList.add();
					tempSize += tempSortedSetsMd.getBufferSize();
				}
				// subKey들을 모두 뺏으므로 key를 삭제
				sortedSet.remove(tempKey);
				*/
				continue;
			}
		}
		
		retHashMap.put("mdList", retMdList);
		retHashMap.put("byteArrayList", retByteArrayList);
		retHashMap.put("size", tempSize);
		
		if(isRemove) {
			for(ManagedData md : retMdList){
				mdList.remove(md);
			}
		}
		System.out.println(curPosition);
		position(curPosition);
		
		return retHashMap;
	}
	
	/**
	 * readBackupMemory 함수 구현
	 * 일정 메모리를 읽어 오는 함수, 읽은 메모리는 지우고 리턴한다.
	 * @param long size
	 * @return HashMap<String, Object>
	 * @author 송원근
	 */
	public HashMap<String, Object> readBackupMemory(long size) {
		long tempSize=0;
		HashMap<String, Object> retHashMap = new HashMap<String, Object>();
		ArrayList<ManagedData> retMdList = new ArrayList<ManagedData>();
		ArrayList<byte[]> retByteArrayList = new ArrayList<byte[]>();
		ManagedData tempMd;
		
		int curPosition = position();
		
		int tempMdListSize = mdBackUpList.size();
		for(int i=0; i < tempMdListSize;i++) {
			if(tempSize >= size) {
				retHashMap.put("mdList", retMdList);
				retHashMap.put("byteArrayList", retByteArrayList);
				retHashMap.put("size", tempSize);
				
				for(ManagedData md : retMdList){
					mdBackUpList.remove(md);
				}
				
//				System.out.println(curPosition);
				position(curPosition);
				
				return retHashMap;
			}
			
			tempMd = mdBackUpList.get(i);
			String tempKey;
			Iterator<ManagedData> it;
			if(tempMd.getType() == 0) {			// keys
				retMdList.add(tempMd);
				// del로 해서 usingMemory를 조절
				retByteArrayList.add(del(tempMd.getPos(), tempMd.getBufferSize()));
				tempSize += tempMd.getBufferSize();
				keys.remove(tempMd.getKey());
//				mdBackUpList.remove(tempMd);
				
			}else if(tempMd.getType() == 1) {	// hashes
				tempKey = tempMd.getKey();
				Collection<ManagedData> col = hashes.get(tempKey).values();
				it = col.iterator();
				
				ManagedData tempHashesMd;
				// hashes는 키당 subKey들이 묶여잇으므로 한번에 처리
				while(it.hasNext()) {
					tempHashesMd = it.next();
					
					retMdList.add(tempHashesMd);
					// del로 해서 usingMemory를 조절
					retByteArrayList.add(del(tempHashesMd.getPos(), tempHashesMd.getBufferSize()));
					tempSize += tempHashesMd.getBufferSize();
					//mdList 에서 삭제
//					mdBackUpList.remove(tempHashesMd);
				}
				// subKey들을 모두 뺏으므로 key를 삭제
				hashes.remove(tempKey);
//				mdBackUpList.remove(tempMd);
				
			}else if(tempMd.getType() == 2) {	// sorted sets
				//directbuffer를 사용하지 않으므로 패스한다.
				
				/*
				tempKey = tempMd.getKey();
				it = sortedSet.get(tempKey).iterator();
				
				ManagedData tempSortedSetsMd;
				while(it.hasNext()) {
					tempSortedSetsMd = it.next();
					
					retMdList.add(tempSortedSetsMd);
					tempScore = tempSortedSetsMd.getSortedSetScore();
					retByteLinkedList.add();
					tempSize += tempSortedSetsMd.getBufferSize();
				}
				// subKey들을 모두 뺏으므로 key를 삭제
				sortedSet.remove(tempKey);
				*/
				continue;
			}
		}
		
		retHashMap.put("mdList", retMdList);
		retHashMap.put("byteArrayList", retByteArrayList);
		retHashMap.put("size", tempSize);
		
		for(ManagedData md : retMdList){
			mdBackUpList.remove(md);
		}
		
		position(curPosition);
		
		return retHashMap;
	}
	
	
	/**
	 * writeMemory 함수 구현
	 * 
	 * @param HashMap<String, Object> hashmap
	 * @return void
	 * @author 송원근
	 */
	public void writeMemory(HashMap<String, Object> hashmap) {
		HashMap<String, Object> retHashMap = new HashMap<String, Object>();
		
		ArrayList<ManagedData> retMdList = (ArrayList)hashmap.get("mdList");
		ArrayList<byte[]> retByteArrayList = (ArrayList<byte[]>) hashmap.get("byteArrayList");
		long size  = (Long) hashmap.get("size");
		
		ManagedData md;
		for(int i=0; i < retMdList.size(); i++) {
			
			int tempPos = put(retByteArrayList.get(i));											//directBuffer put
			md = retMdList.get(i);
//			System.out.println("writeMemory  "+md.toString());
//			md.setPos(retMdList.get(i).getPos());													//md setPos
			md.setPos(tempPos - md.getBufferSize());											//md setPos
//			System.out.println("writeMemory  "+md.toString());
			mdList.add(md);																					// mdList add
			
//			System.out.println(md.getPos() + " pos... "+ md.getBufferSize()+ "size..."+ md.getKey());
			
			
			if(md.getType() == 0) {
				keys.put(md.getKey(), md);										// keys add
			} else if(md.getType() == 1) {
				if(hashes.containsKey(md.getKey())) {
					hashes.get(keys).put(md.getSubKey(), md);		// hashes add
				} else {
					HashMap<String, ManagedData> tempHash = new HashMap<String, ManagedData>();
					tempHash.put(md.getSubKey(), md);
					hashes.put(md.getKey(), tempHash);					// hashes add
				}
			}
			
		}
	}
	
	/**
	 * writeBackUpMemory 함수 구현
	 * 
	 * @param HashMap<String, Object> hashmap
	 * @return void
	 * @author 송원근
	 */
	public void writeBackUpMemory(HashMap<String, Object> hashmap) {
		HashMap<String, Object> retHashMap = new HashMap<String, Object>();
		
		ArrayList<ManagedData> retMdList = (ArrayList)hashmap.get("mdList");
		ArrayList<byte[]> retByteArrayList = (ArrayList<byte[]>) hashmap.get("byteArrayList");
		long size  = (Long) hashmap.get("size");
		
		ManagedData md;
		for(int i=0; i < retMdList.size(); i++) {
			
			int tempPos = putBackUp(retByteArrayList.get(i));										//directBuffer putBackUp
			md = retMdList.get(i);													
			md.setPos(tempPos - md.getBufferSize());													//mdBackUp set pos
			mdBackUpList.add(md);																				// mdBackUpList add
			
			/*
			if(md.getType() == 0) {
				keys.put(md.getKey(), md);										// keys add
			} else if(md.getType() == 1) {
				if(hashes.containsKey(md.getKey())) {
					hashes.get(keys).put(md.getSubKey(), md);		// hashes add
				} else {
					HashMap<String, ManagedData> tempHash = new HashMap<String, ManagedData>();
					tempHash.put(md.getSubKey(), md);
					hashes.put(md.getKey(), tempHash);					// hashes add
				}
			}
			*/
		}
	}
	
	/**
	 * clearBackUpMdList 함수 구현
	 * backup memory clear
	 * @param 
	 * @return void
	 * @author 송원근
	 */
	public void clearBackUpMdList() {
		mdBackUpList.clear();
		usingBackUpMemorySize=0L;
	}
	
	public int getKeyCount() {
		return mdList.size();
	}
	
	public int getBackUpKeyCount() {
		return mdBackUpList.size();
	}
	
	//directBuf
	public byte get() {
		return this.directBuf.get();
	}
	
	public byte get(int index) {
		return this.directBuf.get(index);
	}
	
	public byte[] get(int pos, int size) {
		return this.directBuf.get(pos, size);
	}
	
	public byte del() {
		usingMemorySize--;
		return this.directBuf.del();
	}
	
	public byte del(int index) {
		usingMemorySize--;
		return this.directBuf.del(index);
	}
	
	public byte[] del(int pos, int size) {
		usingMemorySize -= size;
		return this.directBuf.del(pos, size);
	}
	
	public int put(byte b) {
        usingMemorySize++;
		return this.directBuf.put(b);
	}
	
	public int put(byte[] b) {
		usingMemorySize += b.length;
		return this.directBuf.put(b);
	}
	
	public int put(String str) {		//parameter String
		byte[] b = str.getBytes();
		usingMemorySize += b.length;
		return this.directBuf.put(str);
	}
	
	public int put(byte[] b, int pos) {
		usingMemorySize += b.length;
		return this.directBuf.put(b, pos);
	}
	
	
	//backup directBuffer
	public byte delBackUp() {
		usingBackUpMemorySize--;
		return this.directBuf.del();
	}
	
	public byte delBackUp(int index) {
		usingBackUpMemorySize--;
		return this.directBuf.del(index);
	}
	
	public byte[] delBackUp(int pos, int size) {
		usingBackUpMemorySize -= size;
		return this.directBuf.del(pos, size);
	}
	
	public int putBackUp(byte b) {
		usingBackUpMemorySize++;
		return this.directBuf.put(b);
	}
	
	public int putBackUp(byte[] b) {
		usingBackUpMemorySize += b.length;
		return this.directBuf.put(b);
	}
	
	public int putBackUp(String str) {		//parameter String
		byte[] b = str.getBytes();
		usingBackUpMemorySize += b.length;
		return this.directBuf.put(str);
	}
	
	public int putBackUp(byte[] b, int pos) {
		usingBackUpMemorySize += b.length;
		return this.directBuf.put(b, pos);
	}
	
	public int position() {
		return this.directBuf.position();
	}
	
	public Buffer position(int newPosition) {
		return this.directBuf.position(newPosition);
	}
	
	public String directBufferToString() {
		return this.directBuf.toString();
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder("MemoryManager{");
	    sb.append("position=").append(this.position());
	      sb.append(", usingMemorySize=").append(this.getUsingMemorySize());
	    sb.append('}');
	    return sb.toString();
	  }
	
	//getter and setter
	public DirectBuffer getDirectBuf() {
		return directBuf;
	}
	public void setDirectBuf(DirectBuffer directBuf) {
		this.directBuf = directBuf;
	}
	
	public long getUsingMemorySize(){
		return usingMemorySize;
	}
	
	public void setUsingMemorySize(long usingMemorySize) {
		this.usingMemorySize = usingMemorySize;
	}

	public long getUsingBackUpMemorySize() {
		return usingBackUpMemorySize;
	}

	public void setUsingBackUpMemorySize(long usingBackUpMemorySize) {
		this.usingBackUpMemorySize = usingBackUpMemorySize;
	}

	public HashMap<String, ManagedData> getKeys() {
		return keys;
	}

	public void setKeys(HashMap<String, ManagedData> keys) {
		this.keys = keys;
	}
	
	public HashMap<String, HashMap<String, ManagedData>> getHashes() {
		return hashes;
	}

	public void setHashes(
			HashMap<String, HashMap<String, ManagedData>> parentHashes) {
		this.hashes = parentHashes;
	}

	public HashMap<String, SortedSet<ManagedData>> getSortedSet() {
		return sortedSet;
	}

	public void setSortedSet(HashMap<String, SortedSet<ManagedData>> sortedSet) {
		this.sortedSet = sortedSet;
	}

	public ArrayList<ManagedData> getMdList() {
		return mdList;
	}

	public void setMdList(ArrayList<ManagedData> mdList) {
		this.mdList = mdList;
	}

	public ArrayList<ManagedData> getMdBackUpList() {
		return mdBackUpList;
	}

	public void setMdBackUpList(ArrayList<ManagedData> mdBackUpList) {
		this.mdBackUpList = mdBackUpList;
	}

	public ArrayList<ExpireObject> getEpList() {
		return epList;
	}

	public void setEpList(ArrayList<ExpireObject> epList) {
		this.epList = epList;
	}

	public int getCurPosition() {
		return curPosition;
	}

	public void setCurPosition(int curPositon) {
		this.curPosition = curPositon;
	}

	public ArrayList<byte[]> getMemoryDumpLengthList() {
		return memoryDumpLengthList;
	}

	public void setMemoryDumpLengthList(ArrayList<byte[]> memoryDumpLengthList) {
		this.memoryDumpLengthList = memoryDumpLengthList;
	}

	public ArrayList<String> getMemoryDumpKeyList() {
		return memoryDumpKeyList;
	}

	public void setMemoryDumpKeyList(ArrayList<String> memoryDumpKeyList) {
		this.memoryDumpKeyList = memoryDumpKeyList;
	}
	
}
