package org.secmem.greenland.message;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.Queue;

public class MessageManager implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final int LOG_SIZE = 100;
	
	private static final Boolean DEBUG_MODE = false;
	
	private static MessageManager instance;
	
	
	private Queue<String> messageQueue;
	
	public static MessageManager getInstance(){
		if(instance == null){
			instance = new MessageManager();
		}
		return instance;
	}

	public MessageManager(){
		messageQueue = new LinkedList<String>();
	}
	
	public void insertMessage(Object message){
		if(DEBUG_MODE){
			System.out.println(message);
		}
		if(messageQueue.size() > LOG_SIZE){
			messageQueue.remove();
		}
		messageQueue.add(message.toString());
	}
	
	public void insertMessageWithStdOut(Object message){
		insertMessage(message.toString());
		
		System.out.println(message);
		
	}
	
	public void freeMessage(){
		messageQueue.clear();
	}
	
	public String [] getLastLog(int count){
		return (String [])messageQueue.toArray(new String[]{});
	}

}
