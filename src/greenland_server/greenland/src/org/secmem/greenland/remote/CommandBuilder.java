package org.secmem.greenland.remote;

import java.io.IOException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.ServerNotActiveException;
import java.util.ArrayList;
import java.util.HashMap;

import org.secmem.greenland.exception.ServerLockingException;
import org.secmem.greenland.nio.ManagedData;

public interface CommandBuilder extends Remote {
	public int sendCommand(String [] command) throws RemoteException;
	public int sendRawCommand(String command) throws RemoteException;
	
	//Keys
	public Boolean EXISTS(String key) throws RemoteException;
	public int EXPIRE(String key, long liveTime) throws RemoteException, ClassNotFoundException, IOException;
	public Object DEL(String key) throws RemoteException, IOException, ClassNotFoundException, ServerLockingException, ServerNotActiveException;
	public int PERSIST(String key) throws RemoteException, ClassNotFoundException, IOException;
	public int RENAMENX(String key, String newKey) throws RemoteException,ClassNotFoundException, IOException;
	public int SET(String key, Object value) throws RemoteException, IOException, ServerLockingException, ServerNotActiveException;
	public Object GET(String key) throws RemoteException, IOException, ClassNotFoundException;
	
	//Hashes
	public Object HDEL(String key, String subKey) throws RemoteException, IOException, ServerLockingException, ServerNotActiveException;
	public boolean HEXISTS(String key, String subKey) throws RemoteException;
	public Object HGET(String key, String subKey) throws RemoteException, IOException, ClassNotFoundException;
	public int HSET(String key, String subKey, Object value) throws RemoteException, IOException, ServerLockingException, ServerNotActiveException;

	public HashMap<String, Object> HGETALL(String key) throws RemoteException;
	public String [] HKEYS(String key) throws RemoteException;
	public int HLEN(String key) throws RemoteException;
	public ArrayList<HashMap<String, Object>> HMGET(String key, String [] subKeys) throws RemoteException;
	public int HMSET(String key, HashMap<String, Object> data) throws RemoteException;
	public int HSETNX(String key, String subKey) throws RemoteException;
	
	//Sorted Sets
	public int ZADD(String key, String subKey, int value) throws RemoteException;
	public int ZCOUNT(String key) throws RemoteException;
	public String [] ZRANGE(String key, int top, int bottom) throws RemoteException;
	public String [] ZREVRANGE(String key, int top, int bottom) throws RemoteException;
	public ArrayList<HashMap<String, Object>> ZRANGEBYSCORE(String key, int top, int bottom) throws RemoteException;
	public ArrayList<HashMap<String, Object>> ZREVRANGEBYSCORE(String key, int top, int bottom) throws RemoteException;
	public ArrayList<HashMap<String, Object>> ZRANGEWITHSCORE(String key, int top, int bottom) throws RemoteException;

	public int ZRANK(String key, String subKey) throws RemoteException;
	public int ZREVRANK(String key, String subKey) throws RemoteException;
	public Object ZREM(String key, String subKey) throws RemoteException;
	public ArrayList<HashMap<String, Object>> ZREMRANGEBYRANK(String key, String subKey, int top, int bottom) throws RemoteException;
	public ArrayList<HashMap<String, Object>> ZREMREVRANGEBYRANK(String key, String subKey, int top, int bottom) throws RemoteException;	

	//Backup
	public int SETBACKUP(int type, String key, String subKey, Object value) throws RemoteException, IOException;
	public Object GETBACKUP(int type, String key, String subKey) throws RemoteException, IOException;
	public Object DELBACKUP(int type, String key, String subKey) throws RemoteException, IOException;
	
}
