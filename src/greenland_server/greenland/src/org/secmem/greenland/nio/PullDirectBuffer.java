package org.secmem.greenland.nio;

import java.util.ArrayList;
import java.util.Iterator;

public class PullDirectBuffer {
	private MemoryManager mmg;
	private ArrayList<ManagedData> tempMdList;
	private ArrayList<ManagedData> tempMdBackUpList;
	private int tempPosition=0;
	private int tempUsingMemorySize=0;
	private int tempusingBackUpMemorySize=0;
	private ManagedData md;
	private byte b[];
	
	public PullDirectBuffer(MemoryManager mmg) {
		this.mmg = mmg;
		this.tempMdList = new ArrayList<ManagedData>();
		this.tempMdBackUpList = new ArrayList<ManagedData>();
	}
	
	public void pullDirectBuffer(){
		//포지션을 0으로 setting.
		mmg.position(0);

		Iterator<ManagedData> it = mmg.getMdList().iterator();
		// primary data 
		while(it.hasNext()) {
//			md = mmg.getMdList().remove(i);
			md = it.next();
//			System.out.println("pullDirectBuffer : "+ md.toString());
			
			//Sorted Sets 은 directBuffer에 들어가지 않으므로
			if(!(md.getType() == 2)) {
				b = mmg.get(md.getPos(), md.getBufferSize());		//directBuffer에서 byte[]를 get
				mmg.put(b, tempPosition);										//directBuffer에 put

				//mdList에 넣어준다. 포지션값만 바뀌므로 포지션값을 바꿔주고 넣어준다.
				md.setPos(tempPosition);
				tempMdList.add(md);
				
				tempPosition += md.getBufferSize();							//position 증가
				tempUsingMemorySize += md.getBufferSize();
				
			} else {
				//Sorted Set은 그대로 다시 넣어준다.
				tempMdList.add(md);
			}
		}
		
		Iterator<ManagedData> itBack = mmg.getMdBackUpList().iterator();
		//backup data
		while(itBack.hasNext()) {
//			md = mmg.getMdBackUpList().remove(i);
			md = itBack.next();
//			System.out.println("pullDirectBuffer : "+ md.toString());
			
			//sortedSet 은 directBuffer에 들어가지 않으므로
			if(!(md.getType() == 2)) {
				b = mmg.get(md.getPos(), md.getBufferSize());		//directBuffer에서 byte[]를 get
				mmg.putBackUp(b, tempPosition);							//directBuffer에 put

				//mdBackUpList에 넣어준다. 포지션값만 바뀌므로 포지션값을 바꿔주고 넣어준다.
				md.setPos(tempPosition);
				tempMdBackUpList.add(md);
				
				tempPosition += md.getBufferSize();						//position 증가
				tempusingBackUpMemorySize += md.getBufferSize();
				
			} else {
				tempMdBackUpList.add(md);
			}
		}
		mmg.getMdList().clear();
		mmg.getMdBackUpList().clear();
		//MemoryManager 에 새로운 mdList와 mdBackUpList를 넣어준다.
		mmg.setMdList(tempMdList);
		mmg.setMdBackUpList(tempMdBackUpList);
		mmg.setCurPosition(tempPosition);
		mmg.position(tempPosition);
		mmg.setUsingMemorySize(tempUsingMemorySize);
		mmg.setUsingBackUpMemorySize(tempusingBackUpMemorySize);
		
	}
}
