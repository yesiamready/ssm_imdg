package org.secmem.greenland.statemachine;

import java.io.Serializable;

import org.secmem.greenland.message.MessageManager;
import org.secmem.greenland.nio.MemoryManager;
import org.secmem.greenland.remote.InstanceManager;

public class GreenlandStateMachine implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static GreenlandStateMachine instance;
	
	private MemoryManager memoryManager;
	private InstanceManager instanceManager;
	
	private boolean isLocking;
	
	public static GreenlandStateMachine getInstance(){
		if(instance != null){
			return instance;
		}else{
			
			MessageManager.getInstance().insertMessage("State machine error :: not initialized");
			return null;
		}
	}
	
	public static GreenlandStateMachine createInstance(MemoryManager memoryManager, InstanceManager instanceManager){
		instance = new GreenlandStateMachine(memoryManager,instanceManager);
		return instance;
	}
	
	public GreenlandStateMachine(MemoryManager memoryManager, InstanceManager instanceManager){
		super();
		setemoryManager(memoryManager);
		setInstanceManager(instanceManager);
		isLocking = false;
		
	}

	public MemoryManager getMemoryManager() {
		return memoryManager;
	}

	public void setemoryManager(MemoryManager memoryManager) {
		this.memoryManager = memoryManager;
	}

	public InstanceManager getInstanceManager() {
		return instanceManager;
	}

	public void setInstanceManager(InstanceManager instanceManager) {
		this.instanceManager = instanceManager;
	}

	public boolean getIsLocking() {
		return isLocking;
	}

	public void setIsLocking(boolean isLocking) {
		this.isLocking = isLocking;
	}
	
}
