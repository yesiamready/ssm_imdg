package test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.RMISecurityManager;
import java.rmi.server.RMISocketFactory;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import org.secmem.greenland.message.MessageManager;

import connection.GreenlandConnection;

public class TestModule {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		try {
			RMISocketFactory.setSocketFactory( new RMISocketFactory()
			{
			    public Socket createSocket( String host, int port )
			        throws IOException
			    {
			        Socket socket = new Socket();
			        socket.setSoTimeout( 6000 );
			        socket.setSoLinger( false, 0 );
			        socket.connect( new InetSocketAddress( host, port ), 6000 );
			        return socket;
			    }

			    public ServerSocket createServerSocket( int port )
			        throws IOException
			    {
			        return new ServerSocket( port );
			    }
			} );
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		MessageManager manager = new MessageManager();
		
		String [] targetAddrs = {"211.189.20.63"};
		//, "211.189.20.63"
		GreenlandConnection gc = new GreenlandConnection(manager,targetAddrs.length, targetAddrs);
		if(!gc.openConnection()){
			manager.insertMessage("connection open failed");
			return;
		}
		
		if (System.getSecurityManager() == null) { 
			System.setSecurityManager(new RMISecurityManager()); 
		}	
		
		//implement your testcode here
		
		// Keys TEST ////////////////////////////////////////////////////
		
		// TEST SET
//		for(int i=0;i<10;i++){
//			gc.SET("test"+i, i);
//		}
		
		// TEST GET
//		for(int i=0; i<10; i++)
//			System.out.println(gc.GET("test"+i));
		
//		System.out.println(gc.GET("aaa"));
		
		// TEST EXISTS
//		for(int i=0; i<10; i++)
//			System.out.println(gc.EXISTS("test"+i));
		
//		System.out.println(gc.EXISTS("aaa"));
		
		// TEST EXPIRE
//		System.out.println(gc.EXPIRE("test0", 10));
//		System.out.println(gc.GET("test0"));
		
		// TEST DEL
//		System.out.println(gc.DEL("test1"));
//		System.out.println(gc.GET("test1"));
		
		// TEST PERSIST
//		System.out.println(gc.EXPIRE("test2", 10));
//		System.out.println(gc.GET("test2"));
//		System.out.println(gc.PERSIST("test2"));
//		System.out.println(gc.GET("test2"));
		
		// TEST RENAMENX
//		System.out.println(gc.GET("test3"));
//		System.out.println(gc.RENAMENX("test3", "aaa3"));
//		System.out.println(gc.GET("aaa3"));
//		System.out.println(gc.GET("test3"));
		
		
		// Hashes TEST ////////////////////////////////////////////////////
		
		// TEST HSET
//		for(int i=0; i<10; i++)
//			System.out.println(gc.HSET("han", "test"+i, i));
		
		// TEST HGET
//		for(int i=0; i<10; i++)
//			System.out.println(gc.HGET("han", "test"+i));		
		
		// TEST HEXISTS
//		System.out.println(gc.HEXISTS("han", "test0"));
		
		// TEST HGETALL
//		HashMap<String,Object> data = gc.HGETALL("han");
//		
//		Set<String> s1 = null;
//		s1 = data.keySet();
//		
//		Object value = null;		
//		for(Object hkey : s1) {
//			
//			try {
//				byte[] ret1 = (byte [])data.get(hkey);
//				ByteArrayInputStream in = new ByteArrayInputStream(ret1);
//				ObjectInput is = new ObjectInputStream(in);
//				
//				value = is.readObject();
//			
//			} catch(Exception e) {
//				
//				e.printStackTrace();
//			}
//						
//			System.out.println(hkey + " :: " + value);
//		}		
		
		// TEST HLEN
//		System.out.println(gc.HLEN("han"));
		
		// TEST HKEYS
//		String[] str = gc.HKEYS("han");
//		for(int i=0; i<str.length; i++) {
//			
//			System.out.println(str[i]);
//		}
		
		
		// TEST HDEL
//		System.out.println(gc.HGET("han", "test1"));
//		System.out.println(gc.HDEL("han", "test1"));
//		System.out.println(gc.HGET("han", "test1"));
		
		
		// Sorted Sets TEST ////////////////////////////////////////////////////
		
		// TEST ZADD
//		gc.ZADD("myzset", "a", 1);
//		gc.ZADD("myzset", "b", 2);
//		System.out.println(gc.ZADD("myzset", "c", 3)); 
		
		// TEST ZCOUNT
//		System.out.println(gc.ZCOUNT("myzset")); 
		
		// TEST ZRANGE
//		String []str = gc.ZRANGE("myzset", 0, 2);
//		for(int i=0; i<str.length; i++) {
//			
//			System.out.println(str[i]);
//		}
		
		// TEST ZRANGEBYSCORE		
//		ArrayList<HashMap<String,Object>> alist = gc.ZRANGEBYSCORE("myzset", 0, 2);
//		HashMap<String,Object> data = null;
//		
//		Set<String> s1 = null;
//		for(int i=0; i<alist.size(); i++) {
//			
//			data = alist.get(i);
//			s1 = data.keySet();
//			
//			for(Object hkey : s1) {
//				System.out.println(hkey);
//			}			
//		}
		
		// TEST ZRANGEWITHSCORE
		
		
		// TEST ZRANK
//		System.out.println(gc.ZRANK("myzset", "a"));
		
		// TEST ZREM
//		System.out.println(gc.ZREM("myzset", "a")); 
		
		
		
	}

}
