package  org.secmem.greenland.datastructure;

public class ExpireObject {
	private int type;
	private String key;
	private long expireTime;
	
	public ExpireObject(int type, String key, long expireTime) {
		super();
		this.type = type;
		this.key = key;
		this.expireTime = expireTime;
	}
	
	public ExpireObject() {
		super();
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public long getExpireTime() {
		return expireTime;
	}

	public void setExpireTime(long expireTime) {
		this.expireTime = expireTime;
	}
	
}
