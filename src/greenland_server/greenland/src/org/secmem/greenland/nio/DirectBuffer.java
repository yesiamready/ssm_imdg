package  org.secmem.greenland.nio;

import java.nio.Buffer;
import java.nio.ByteBuffer;

public final class DirectBuffer {
	ByteBuffer directBuffer;
	
	
	public DirectBuffer() {
		this.directBuffer = ByteBuffer.allocateDirect(1024*1024*1024);	//1GB
	}
	
	public DirectBuffer(int size) {
		this.directBuffer = ByteBuffer.allocateDirect(size);
	}
	
	public DirectBuffer compact() {
		directBuffer.compact();
		return this;
	}
	
	public byte get() {
		return directBuffer.get();
	}	
	
	public byte get(int index) {
		return directBuffer.get(index);
	}
	
	public byte[] get(int pos, int size){
		byte[] retByte = new byte[size];
		//끝의 포지션을 가지고 있는다.
		int tempPosition = directBuffer.position();
		//읽어야 포지션으로 이동
		directBuffer.position(pos);
		//읽어야할 포지션부터 size 만큼 읽는다.
		directBuffer.get(retByte, 0, size);	//pos은 retByte의 처음 pos 위치
//		MessageManager.getInstance().insertMessage("position : "+ directBuffer.position()+ "  size : "+ size);
		//끝의 포지션으로 다시 이동한다. 나중에 다시 put했을때 뒤에 들어가게 하기위해서
		directBuffer.position(tempPosition);
		return retByte;
	}
	
	public byte del() {
		return directBuffer.get();
	}	
	
	public byte del(int index) {
		return directBuffer.get(index);
	}
	
	public byte[] del(int pos, int size){
		byte[] retByte = new byte[size];
		directBuffer.position(pos);
		directBuffer.get(retByte, 0, size);	//pos은 retByte의 처음 pos 위치
//		MessageManager.getInstance().insertMessage("position : "+ directBuffer.position()+ "  size : "+ size);
		return retByte;
	}
	
	public int put(byte b) {
        ensureSize(1);
        directBuffer.put(b);
        return directBuffer.position();
    }
	
	public int put(byte[] b) {
		ensureSize(b.length);
		directBuffer.put(b);
		return directBuffer.position();
	}
	
//	public int put(int bufferSize) {
////		ensureSize(b.length);
////		directBuffer.put(b);
//		byte[] b
//		return directBuffer.position();
//	}
	
	//parameter String
	public int put(String str) {
		byte[] b = str.getBytes();
		ensureSize(b.length);
		directBuffer.put(b);
		return directBuffer.position();
	}
	
	public int put(byte[] b, int pos) {
		ensureSize(b.length);
		directBuffer.position(pos);		//set Position
		directBuffer.put(b);
		//포지션이 어딘지 확인 해주는 거 해봐야댐
		return directBuffer.position();
	}
	
	private boolean ensureSize(int i) {
        check();
        if (directBuffer.remaining() < i) {
            return true;
        }else {
        	return false;
        }
    }
	
//	private void ensureSize(int i) {
//        check();
//        if (directBuffer.remaining() < i) {
//            int newCap = Math.max(directBuffer.limit() << 1, directBuffer.limit() + i);
//            ByteBuffer newBuffer = directBuffer.isDirect() ? ByteBuffer.allocateDirect(newCap) : ByteBuffer.allocate(newCap);
//            newBuffer.order(directBuffer.order());
//            directBuffer.flip();
//            newBuffer.put(directBuffer);
//            directBuffer = newBuffer;
//        }
//    }
	
	public int position() {
	    check();
	    return this.directBuffer.position();
	 }
	  
	//return type Buffer??????????????????????????????????
	 public Buffer position(int newPosition) {
	   check();
	   return this.directBuffer.position(newPosition);
	 }
	
	private void check() {
        if (directBuffer == null) {
            throw new IllegalStateException("Buffer is closed!");
        }
    }

	public String toString() {
		StringBuilder sb = new StringBuilder("DirectBuffer{");
	    if (this.directBuffer != null) {
	      sb.append("position=").append(this.directBuffer.position());
	      sb.append(", limit=").append(this.directBuffer.limit());
	      sb.append(", capacity=").append(this.directBuffer.capacity());
	      sb.append(", order=").append(this.directBuffer.order());
	    } else {
	      sb.append("<CLOSED>");
	    }
	    sb.append('}');
	    return sb.toString();
	  }
}

class DirectBufferThread implements Runnable {
	DirectBuffer directBuf;
	
	public void run() {
		directBuf = new DirectBuffer();
	}
	
}

