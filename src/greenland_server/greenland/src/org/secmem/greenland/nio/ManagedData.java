package  org.secmem.greenland.nio;

import java.io.Serializable;

//받은 Data class를 관리하는 Data클래스

public class ManagedData implements Comparable, Serializable {
	// mplements DataSerializable {

	// public static final int FACTORY_ID = 0;
	// public static final int ID = 0;
	// public static final int NO_CLASS_ID = 0;
	// cannot be zero.

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int type;			// 0 : Keys, 1 : Hashes, 2 : Sorted Sets
	private String key;			//key
	private String subKey;
	// private byte[] buffer; 	//data 는 directBuffer로 들어감
	private int bufferSize;
	private int pos;
	private boolean expireFlag;
	private int useCount=0;
	private int sortedSetScore;

	public ManagedData() {
	}
	
	public ManagedData(int type, int bufferSize, int pos, String key) {
		super();
		this.type = type;
		// this.buffer = bytes;
		// this.bufferSize = (byte)this.buffer.length; //int byte로 형변환 크기 그대로?
		this.bufferSize = bufferSize;
		this.pos = pos;
		this.key = key;
		this.subKey = null;
		expireFlag = false;
	}

	public ManagedData(int type, String str, int pos, String key) {
		super();
		this.type = type;
		// this.buffer = str.getBytes();
		// this.bufferSize =(byte)this.buffer.length;
		this.bufferSize = (byte) str.getBytes().length;
		this.pos = pos;
		this.key = key;
		this.subKey = null;
		expireFlag = false;
	}
	
	public ManagedData(int type, int bufferSize, int pos, String key, String subKey) {
		super();
		this.type = type;
		// this.buffer = str.getBytes();
		// this.bufferSize =(byte)this.buffer.length;
		this.bufferSize = bufferSize;
		this.pos = pos;
		this.key = key;
		this.subKey = subKey;
		expireFlag = false;
	}
	
	//SortedSet 
	public ManagedData(int type, String key, String subKey, int sortedSetScore) {
		super();
		this.type = type;
		this.key = key;
		this.subKey = subKey;
		this.sortedSetScore = sortedSetScore;
	}
	
	//LFUManagedData return, 생성자에서 쓰려고
	public ManagedData(int type, String key, String subKey, int bufferSize, int useCount) {
		this.type = type;
		this.key = key;
		this.subKey = subKey;
		this.bufferSize = bufferSize;
		this.useCount = useCount;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getBufferSize() {
		return bufferSize;
	}

	public void setBufferSize(int bufferSize) {
		this.bufferSize = bufferSize;
	}

	public int getPos() {
		return pos;
	}

	public void setPos(int pos) {
		this.pos = pos;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
	
	public String getSubKey() {
		return subKey;
	}

	public void setSubKey(String subKey) {
		this.subKey = subKey;
	}

	public boolean isExpireFlag() {
		return expireFlag;
	}

	public void setExpireFlag(boolean expireFlag) {
		this.expireFlag = expireFlag;
	}

	public int getUseCount() {
		return useCount;
	}

	public void setUseCount(int useCount) {
		this.useCount = useCount;
	}
	
	public int getSortedSetScore() {
		return sortedSetScore;
	}

	public void setSortedSetScore(int sortedSetScore) {
		this.sortedSetScore = sortedSetScore;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder("ManagedData{");
		// if (this.directBuffer != null)
		{
			sb.append("type=").append(this.getType());
			sb.append(", key=").append(this.getKey());
			sb.append(", subKey=").append(this.getSubKey());
			sb.append(", position=").append(this.getPos());
			sb.append(", bufferSize=").append(this.getBufferSize());
			sb.append(", useCount=").append(this.getUseCount());
		}
		// else
		{
			// sb.append("<CLOSED>");
			// }
			sb.append('}');
			return sb.toString();
		}
	}

	public int compareTo(Object o) {
		ManagedData md = (ManagedData)o;
		
		if (md.sortedSetScore > sortedSetScore)
			return -1;
		else if (md.sortedSetScore == sortedSetScore)
			return 0;
		else 
			return 1;
		
	}
	
}