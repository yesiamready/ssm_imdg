import java.rmi.Remote;
import java.rmi.RemoteException;


public interface ChatDualClient extends Remote{
	public void test(String msg) throws RemoteException;

}
