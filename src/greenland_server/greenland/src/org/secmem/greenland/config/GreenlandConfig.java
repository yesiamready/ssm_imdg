package org.secmem.greenland.config;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.secmem.greenland.message.MessageManager;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class GreenlandConfig {
	private static GreenlandConfig instance;
	
	private int instanceCount;
	public int getInstanceCount(){
		return instanceCount;
	}
	
	private String backupStrategy;
	public String getBackupStrategy(){
		return backupStrategy;
	}
	
	private List<ServerInfo> serverList;
	public List<ServerInfo> getServerList(){
		return serverList;
	}
	
	private ServerInfo localServerInfo;
	
	private String uuid;
	public String getUUID(){
		return uuid;
	}
	
	
	public static GreenlandConfig getInstance() throws SAXException, IOException, ParserConfigurationException{
		if(instance == null)
			instance = new GreenlandConfig();
		return instance;
	}
	
	public GreenlandConfig() throws SAXException, IOException, ParserConfigurationException{
		InputStream is = GreenlandConfig.class.getResourceAsStream("serverinfo.xml");
		
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(is);
		
		uuid = UUID.randomUUID().toString();
		MessageManager.getInstance().insertMessage("  - UUID :: " + uuid);
		
		Element serverCount =  (Element)doc.getElementsByTagName("ServerCount").item(0);
		MessageManager.getInstance().insertMessage("  -  Server count :: " + serverCount.getAttribute("Count"));
		
		instanceCount = Integer.parseInt(serverCount.getAttribute("Count"));
		
		
		Element backupSetting = (Element)doc.getElementsByTagName("BackupSetting").item(0);
		MessageManager.getInstance().insertMessage("  -  Backup Strategy :: " + backupSetting.getAttribute("Strategy"));
		
		backupStrategy = backupSetting.getAttribute("Strategy");
		
		
		NodeList nList = doc.getElementsByTagName("ServerInfo");
		
		serverList = new ArrayList<ServerInfo>();
		
		try{
			for(int i=0;i<instanceCount;i++){
				Element n = (Element)nList.item(i);
				int backupTarget = Integer.parseInt(n.getElementsByTagName("backupindex").item(0).getTextContent());
				int primaryIndex = Integer.parseInt(n.getElementsByTagName("index").item(0).getTextContent());
				if(backupTarget >= instanceCount){
					backupTarget %= instanceCount;
				}
				
				if(instanceCount == 1){
					backupTarget =primaryIndex; 
				}
				
				if(n.getElementsByTagName("Addr").item(0).getTextContent().equals(InetAddress.getLocalHost().getHostAddress())){
					setLocalServerInfo(new ServerInfo(n.getElementsByTagName("Alias").item(0).getTextContent(), 
							InetAddress.getByName(n.getElementsByTagName("Addr").item(0).getTextContent()), 
							Integer.parseInt(n.getElementsByTagName("Size").item(0).getTextContent()), primaryIndex, backupTarget ));
					System.out.printf("  -  Config info :: <localhost>server%d:: serverIndex: %d, backupIndex: %d, Alias: %s, Addr: %s, Capacity: %s\n", i, i, backupTarget,n.getElementsByTagName("Alias").item(0).getTextContent(), n.getElementsByTagName("Addr").item(0).getTextContent(), n.getElementsByTagName("Size").item(0).getTextContent());
					continue;
				}
					
				
				
				ServerInfo temp = new ServerInfo(n.getElementsByTagName("Alias").item(0).getTextContent(), 
						InetAddress.getByName(n.getElementsByTagName("Addr").item(0).getTextContent()), 
						Integer.parseInt(n.getElementsByTagName("Size").item(0).getTextContent()), primaryIndex, backupTarget);
				
				serverList.add(temp);
				
				System.out.printf("  -  Config info :: <remote>server%d:: serverIndex: %d, backupIndex: %d, Alias: %s, Addr: %s, Capacity: %s\n", i,i, backupTarget,n.getElementsByTagName("Alias").item(0).getTextContent(), n.getElementsByTagName("Addr").item(0).getTextContent(), n.getElementsByTagName("Size").item(0).getTextContent());
			}
			
		
		}catch(Exception e){
			e.printStackTrace();
		}
	}


	public ServerInfo getLocalServerInfo() {
		return localServerInfo;
	}


	public void setLocalServerInfo(ServerInfo localServerInfo) {
		this.localServerInfo = localServerInfo;
	}

}