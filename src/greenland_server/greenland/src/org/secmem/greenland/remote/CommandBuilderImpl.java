package org.secmem.greenland.remote;

import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.ServerNotActiveException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;

import org.secmem.greenland.datastructure.BackUpProcess;
import org.secmem.greenland.datastructure.HashesProcess;
import org.secmem.greenland.datastructure.KeysProcess;
import org.secmem.greenland.datastructure.SortedSetsProcess;
import org.secmem.greenland.exception.ServerLockingException;
import org.secmem.greenland.message.MessageManager;
import org.secmem.greenland.nio.ManagedData;
import org.secmem.greenland.nio.MemoryManager;
import org.secmem.greenland.statemachine.GreenlandStateMachine;

public class CommandBuilderImpl extends UnicastRemoteObject implements
		CommandBuilder {
	
	private KeysProcess keyProcess;
	private HashesProcess hashProcess;
	private SortedSetsProcess sortedSetProcess;
	private BackUpProcess backupProcess;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CommandBuilderImpl() throws RemoteException {
		super();
		keyProcess = new KeysProcess(GreenlandStateMachine.getInstance().getMemoryManager());
		hashProcess = new HashesProcess(GreenlandStateMachine.getInstance().getMemoryManager());
		sortedSetProcess = new SortedSetsProcess(GreenlandStateMachine.getInstance().getMemoryManager());
		backupProcess = new BackUpProcess(GreenlandStateMachine.getInstance().getMemoryManager());
	}

	@Override
	public int sendCommand(String[] command) throws RemoteException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int sendRawCommand(String command) throws RemoteException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Boolean EXISTS(String key) throws RemoteException {
		MessageManager.getInstance().insertMessage("EXIST :: " + key);
		return keyProcess.EXISTS(key);
		
	}


	@Override
	public int EXPIRE(String key, long liveTime) throws RemoteException, ClassNotFoundException, IOException {
		MessageManager.getInstance().insertMessage("EXPIRE :: " + key);
		return keyProcess.EXPIRE(key, liveTime);
	}	

	@Override
	public Object DEL(String key) throws RemoteException, IOException, ClassNotFoundException, ServerLockingException, ServerNotActiveException {
		MessageManager.getInstance().insertMessage("DEL :: " + key);
		ManagedData md =(ManagedData)keyProcess.DEL(key);
		byte[] ret1 = GreenlandStateMachine.getInstance().getMemoryManager().get(md.getPos(), md.getBufferSize());

		if(GreenlandStateMachine.getInstance().getInstanceManager().getInstanceCount() == 1)
			return ret1;
		
		InstanceManager im = GreenlandStateMachine.getInstance().getInstanceManager();
		
		InstanceConnection backupConn = im.getInstance(im.getLocalInstance().getServerInfo().getBackupServerIndex());
		backupConn.getCommandBuilder().DELBACKUP(MemoryManager.TYPE_KEYS, key, null);
		
		return ret1;
		
	}

	@Override
	public int PERSIST(String key) throws RemoteException, ClassNotFoundException, IOException {
		MessageManager.getInstance().insertMessage("PERSIST :: " + key);
		return keyProcess.PERSIST(key);
	}


	@Override
	public int RENAMENX(String key, String newKey) throws RemoteException,ClassNotFoundException, IOException {
		MessageManager.getInstance().insertMessage("RENAMENX :: " + key);
		return keyProcess.RENAMENX(key, newKey);
	}	
	

	@Override
	public int SET(String key, Object value) throws RemoteException,
			IOException, ServerLockingException, ServerNotActiveException {
		MessageManager.getInstance().insertMessage("SET :: " + key);
		
		int ret = keyProcess.SET(key, value);
		
		if(GreenlandStateMachine.getInstance().getInstanceManager().getInstanceCount() == 1)
			return ret;
		
		InstanceManager im = GreenlandStateMachine.getInstance().getInstanceManager();
		InstanceConnection backupConn = im.getInstance(im.getLocalInstance().getServerInfo().getBackupServerIndex());
		backupConn.getCommandBuilder().SETBACKUP(MemoryManager.TYPE_KEYS,key,null, value);
		
		return ret;
	}

	@Override
	public Object GET(String key) throws RemoteException, IOException, ClassNotFoundException {
		MessageManager.getInstance().insertMessage("GET :: " + key);
		ManagedData md =(ManagedData)keyProcess.GET(key);
		byte[] ret1 = GreenlandStateMachine.getInstance().getMemoryManager().get(md.getPos(), md.getBufferSize());
				
		return ret1;
	}

	@Override
	public Object HDEL(String key, String subKey) throws IOException, ServerLockingException, ServerNotActiveException, RemoteException {
		MessageManager.getInstance().insertMessage("HDEL :: " + key + ", "+subKey);
		
		ManagedData md =(ManagedData)hashProcess.HDEL(key, subKey);
		byte[] ret1 = GreenlandStateMachine.getInstance().getMemoryManager().get(md.getPos(), md.getBufferSize());
		
		if(GreenlandStateMachine.getInstance().getInstanceManager().getInstanceCount() == 1)
			return ret1;
		
		InstanceManager im = GreenlandStateMachine.getInstance().getInstanceManager();
		
		InstanceConnection backupConn = im.getInstance(im.getLocalInstance().getServerInfo().getBackupServerIndex());
		
		backupConn.getCommandBuilder().DELBACKUP(MemoryManager.TYPE_HASHES, key, subKey);
		
		return ret1;
	}

	@Override
	public boolean HEXISTS(String key, String subKey) throws RemoteException {
		MessageManager.getInstance().insertMessage("HEXISTS :: " + key + ", "+subKey);
		
		return hashProcess.HEXISTS(key, subKey);
	}

	@Override
	public Object HGET(String key, String subKey) throws RemoteException,IOException, ClassNotFoundException {
		MessageManager.getInstance().insertMessage("HGET :: " + key + ", "+subKey);
		
		ManagedData md =(ManagedData)hashProcess.HGET(key, subKey);
		byte[] ret1 = GreenlandStateMachine.getInstance().getMemoryManager().get(md.getPos(), md.getBufferSize());
		
		return ret1;
	}

	@Override
	public int HSET(String key, String subKey, Object value)
			throws RemoteException,IOException, ServerLockingException, ServerNotActiveException {
		MessageManager.getInstance().insertMessage("HSET :: " + key + ", "+subKey);
		
		int ret = hashProcess.HSET(key, subKey, value);
		
		if(GreenlandStateMachine.getInstance().getInstanceManager().getInstanceCount() == 1)
			return ret;
		
		InstanceManager im = GreenlandStateMachine.getInstance().getInstanceManager();
		
		InstanceConnection backupConn = im.getInstance(im.getLocalInstance().getServerInfo().getBackupServerIndex());
		
		backupConn.getCommandBuilder().SETBACKUP(MemoryManager.TYPE_HASHES,key, subKey, value);
		
		// TODO Auto-generated method stub
		return ret;
	}

	@Override
	public HashMap<String,Object> HGETALL(String key)
			throws RemoteException {
		
		HashMap<String, ManagedData> hash = hashProcess.HGETALL(key);
		
		HashMap<String, Object> ret = new HashMap<String, Object>();
		
		MemoryManager memoryManager = GreenlandStateMachine.getInstance().getMemoryManager();
		
		for(String k : hash.keySet()){
			ManagedData md = hash.get(k);
			
			ret.put(k, memoryManager.get(md.getPos(), md.getBufferSize()));
		}
		
		return ret;
	}

	@Override
	public String[] HKEYS(String key) throws RemoteException {
		// TODO Auto-generated method stub
		return hashProcess.HKEYS(key);
	}

	@Override
	public int HLEN(String key) throws RemoteException {
		// TODO Auto-generated method stub
		return hashProcess.HLEN(key);
	}

	@Override
	public ArrayList<HashMap<String, Object>> HMGET(String key, String[] subKeys)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int HMSET(String key, HashMap<String, Object> data)
			throws RemoteException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int HSETNX(String key, String subKey) throws RemoteException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int ZCOUNT(String key) throws RemoteException {
		return sortedSetProcess.ZCARD(key);
	}

	@Override
	public String[] ZRANGE(String key, int top, int bottom)
			throws RemoteException {
		return sortedSetProcess.ZRANGE(key, top, bottom);
	}

	@Override
	public String[] ZREVRANGE(String key, int top, int bottom)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public ArrayList<HashMap<String, Object>> ZRANGEWITHSCORE(String key,
			int top, int bottom) throws RemoteException {
		// TODO Auto-generated method stub
		ArrayList<Object> mdList = new ArrayList<Object>();
		mdList = sortedSetProcess.ZRANGEWITHSCORE(key, top, bottom);
		
		ArrayList<HashMap<String, Object>> ret = new ArrayList<HashMap<String,Object>>();
		
		for(Object o : mdList){
			HashMap<String, Object> temp = new HashMap<String, Object>();
			ManagedData md  = (ManagedData)o;
			temp.put(md.getSubKey(), md.getSortedSetScore());
			ret.add(temp);
		}
		
		return ret;  
	}	
	
	@Override
	public ArrayList<HashMap<String, Object>> ZRANGEBYSCORE(String key,
			int top, int bottom) throws RemoteException {
		ArrayList<Object> mdList = new ArrayList<Object>();
		mdList = sortedSetProcess.ZRANGEBYSCORE(key, top, bottom);
		
		ArrayList<HashMap<String, Object>> ret = new ArrayList<HashMap<String,Object>>();
		
		for(Object o : mdList){
			HashMap<String, Object> temp = new HashMap<String, Object>();
			ManagedData md  = (ManagedData)o;
			temp.put(md.getSubKey(), md.getSortedSetScore());
			ret.add(temp);
		}
		
		return ret;  
	}

	@Override
	public ArrayList<HashMap<String, Object>> ZREVRANGEBYSCORE(String key,
			int top, int bottom) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int ZRANK(String key, String subKey) throws RemoteException {
		return sortedSetProcess.ZRANK(key, subKey);
	}

	@Override
	public int ZREVRANK(String key, String subKey) throws RemoteException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Object ZREM(String key, String subKey) throws RemoteException {
		ManagedData md  = (ManagedData)sortedSetProcess.ZREM(key, subKey);
		return md.getSortedSetScore();
	}

	@Override
	public ArrayList<HashMap<String, Object>> ZREMRANGEBYRANK(String key,
			String subKey, int top, int bottom) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<HashMap<String, Object>> ZREMREVRANGEBYRANK(String key,
			String subKey, int top, int bottom) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int ZADD(String key, String subKey, int value)
			throws RemoteException {
		
		return sortedSetProcess.ZADD(key, subKey, value);
	}
	

	@Override
	public int SETBACKUP(int type, String key, String subKey, Object value) throws RemoteException,
			IOException {
		return backupProcess.SETBACKUP(type, key, subKey, value);
	}

	@Override
	public Object GETBACKUP(int type, String key, String subKey) throws RemoteException, IOException {
		return backupProcess.GETBACKUP(type, key, subKey);
	}

	@Override
	public Object DELBACKUP(int type, String key, String subKey) throws RemoteException, IOException {
		return backupProcess.DELBACKUP(type, key, subKey);
	}




}
