package org.secmem.greenland.pool;

import java.net.InetAddress;
import java.rmi.Naming;
import java.util.List;

import org.secmem.greenland.cluster.ServerModificationAction;
import org.secmem.greenland.config.GreenlandConfig;
import org.secmem.greenland.config.ServerInfo;
import org.secmem.greenland.message.MessageManager;
import org.secmem.greenland.remote.InstanceConnection;
import org.secmem.greenland.remote.InstanceManager;
import org.secmem.greenland.remote.InstanceManagerImpl;
import org.secmem.greenland.statemachine.GreenlandStateMachine;

public class ConnectionPoolManager implements Runnable {
	private InstanceManager instanceManager;
	public ConnectionPoolManager(InstanceManager manager){
		instanceManager = manager;
	}
	
	@Override
	public void run() {
		try {
			int tryCount = 0;
			while(true){
				if(tryCount > 30)
					break;
				tryCount++;
				MessageManager.getInstance().insertMessage("try to bind other server ... "+tryCount+"/30 ");
				GreenlandConfig conf = GreenlandConfig.getInstance();
				List<ServerInfo> serverList = conf.getServerList();

				int disconnCount = 0;
				
				MessageManager.getInstance().insertMessage("serverList Size :: "+serverList.size());
				for(int i=0;i<serverList.size();i++){
					try{
						if(instanceManager.findInstance(serverList.get(i).getServerAddr()) != null)
							continue;
					}catch(Exception e){
						MessageManager.getInstance().insertMessage("  -  fail to connect ... " + serverList.get(i).getAlias()+" : "+serverList.get(i).getServerAddr().getHostAddress());
						MessageManager.getInstance().insertMessage("  -  err msg: " +e.getMessage());
					}
					
					
					if(serverList.get(i).getServerAddr().getHostAddress().equalsIgnoreCase(InetAddress.getLocalHost().getHostAddress()))
						continue;
					
					MessageManager.getInstance().insertMessage(serverList.get(i).getServerAddr().getHostAddress()+"/"+InetAddress.getLocalHost().getHostAddress());
					
					++disconnCount;
					
					MessageManager.getInstance().insertMessage("  -  start to connect ... " + serverList.get(i).getAlias()+" : "+serverList.get(i).getServerAddr().getHostAddress());
					try{
						InstanceConnection conn = null;
						conn = (InstanceConnection)Naming.lookup("//"+serverList.get(i).getServerAddr().getHostAddress() + "/GreenlandInstanceConnection");
						MessageManager.getInstance().insertMessage("current " + i + "th server info :: " + conn.getServerInfo().toString());
						instanceManager.addInstance(conn);
						MessageManager.getInstance().insertMessage("  -  //"+serverList.get(i).getServerAddr().getHostAddress() + "/GreenlandInstanceConnection");			
						MessageManager.getInstance().insertMessage("  -  connect successfully... ");
					}catch(Exception e){
						MessageManager.getInstance().insertMessage("  -  fail to connect ... " + serverList.get(i).getAlias()+" : "+serverList.get(i).getServerAddr().getHostAddress());
						MessageManager.getInstance().insertMessage("  -  err msg: " +e.getMessage());
					}
					
				}
				
				if(disconnCount == 0)
					break;
				
				Thread.sleep(3000);
			}
			
			MessageManager.getInstance().insertMessage("binding all server ... ");
			
			while(true){
				int result = instanceManager.checkKeepAlive();
				if(result != InstanceManagerImpl.KEEPALIVE_OK){
					MessageManager.getInstance().insertMessage("Server "+result+" has halted");
					
					if(GreenlandStateMachine.getInstance().getInstanceManager().getLocalInstance().getServerInfo().getServerIndex() == 0){
						ServerModificationAction.getInstance().exceptInstance(result);
					}else{
						instanceManager.removeInstance(result);
					}
				}
				
				
				
				Thread.sleep(3000);
			}
			
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

}
